import React, { Component } from 'react';
import './Sidebar.css';

class Sidebar extends Component {
    render() {
        return (
            <nav className="Sidebar">
                <ul>
                    <li className="list-items">Обучающее видео</li>
                    <li className="list-items">Оформление заказа</li>
                    <li className="list-items">Оплата</li>
                    <li className="list-items">Доставка</li>
                    <li className="list-items">Гарантия</li>
                    <li className="list-items">Возврат</li>
                    <li className="list-items">Контакты</li>
                    <li className="list-items">Партнерам</li>
                </ul>
            </nav>
        )
    }
}

export default Sidebar
