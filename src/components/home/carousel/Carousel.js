import React, { Component } from 'react';
import './Carousel.css';

class Carousel extends Component {
    constructor(props){
        super(props)
    }

    render() {
        return (
            <div id="carouselExampleIndicators" className="carousel slide" >
                <ol className="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active dots-carousel"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1" className="dots-carousel"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2" className="dots-carousel"></li>
                </ol>
                <div className="carousel-inner">
                    {
                        this.props.images.map((url, i) => {
                            return (
                                <div style={{backgroundImage: `url('${url}')`, color: 'white'}} className={"carousel-item " + (i == 0 ? 'active' : '')} key={i}>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Carousel
