import React from "react";
import './Data.css';

export default () => (
    <ul className="lists-data">
        <li className="d-flex justify-content-between list-data">
            <div className="row row-data">
                <span className="col-sm-6 left">Класс:</span>
                <span className="col-sm-6">Standart</span>
            </div>
        </li>
        <li className="d-flex justify-content-between list-data">
            <div className="row row-data">
                <span className="col-sm-6 left">Потребляемая мощность:</span>
                <span className="col-sm-6">59 Вт.</span>
            </div>
        </li>
        <li className="d-flex justify-content-between list-data">
            <div className="row row-data">
                <span className="col-sm-6 left">Сила света:</span>
                <span className="col-sm-6">3459 Люмен = 7,5 ламп накаливания по 40 Вт.</span>
            </div>
        </li>
        <li className="d-flex justify-content-between list-data">
            <div className="row row-data">
                <span className="col-sm-6 left">Гарантия:</span>
                <span className="col-sm-6">3 года</span>
            </div>
        </li>
        <li className="d-flex justify-content-between list-data">
            <div className="row row-data">
                <span className="col-sm-6 left">Монтаж:</span>
                <span className="col-sm-6">да</span>
            </div>
        </li>
        <li className="d-flex justify-content-between list-data">
            <div className="row row-data">
                <span className="col-sm-6 left">Итого сумма:</span>
                <span className="col-sm-6">2594 рублей</span>
            </div>
        </li>
    </ul>
)
