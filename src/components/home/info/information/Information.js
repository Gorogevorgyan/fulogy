import React, { Component } from "react";
import './Information.css';

class  Information extends Component {
    constructor(){
        super()
    }

    openInformation() {
        const $text = document.getElementsByClassName('text-information')[0];
        const $menu = document.getElementsByClassName('mobile-close-text')[0]
        $menu.classList.remove('close-text')
        $text.classList.remove('close-text')
    }

    render() {
        return (
            <div className="Information d-flex">
                <div className="d-flex justify-content-center align-items-center left-section" onClick={this.openInformation}>i</div>
                <div className="d-flex justify-content-center align-items-center right-section">Выберите цвет свечения</div>
            </div>
        )
    }

}

export default Information
