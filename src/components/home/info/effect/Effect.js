import React, { Component } from 'react';
import './Effect.css';

class Effect extends Component {
    constructor(props){
        super(props)
        this.state = {
            active: 0,
            imgEffects: [
                './img/cold.jpg',
                './img/day.jpg',
                './img/warm.jpg'
            ]
        }
    }

    onChangeCarousel(active) {
        //Since we do not have the address to receive this information, I have prepared a random arrays
        const arr = [
            'https://upload.wikimedia.org/wikipedia/commons/d/d5/Tulips_10_IMG.jpg',
            'https://tinyjpg.com/images/social/website.jpg',
            'https://upload.wikimedia.org/wikipedia/commons/6/67/USS_Bowfin_img.JPG',
        ];

        const images = [
            './img/cold.jpg',
            './img/day.jpg',
            './img/warm.jpg'
        ];

        const newImg = [
            'https://www.codeproject.com/KB/GDI-plus/ImageProcessing2/img.jpg',
            'https://onlinejpgtools.com/images/examples-onlinejpgtools/smile.jpg',
            'https://d2ci88w16yaf6n.cloudfront.net/external_assets/tutorials/artwork_logos_v1/soldier.png'
        ];

        switch (active) {
            case 0:
                this.props.changes(images);
                break;
            case 1:
                this.props.changes(arr);
                break;
            case 2:
                this.props.changes(newImg);
                break;
        }
        this.setState({active})
    }

    render() {
        return (
            <div className="Effect d-flex">
                <div className="row d-flex align-items-center justify-content-between scroll-touch">
                    {
                        this.state.imgEffects.map((url, i) => {
                            return (
                                <div key={i} className="effect-img col-sm-12 col-md-4" onClick={this.onChangeCarousel.bind(this, i)}>
                                    {
                                        i == this.state.active
                                            ? <span><i className="far fa-check-square chec-icon"></i></span>
                                            : ''
                                    }
                                    <img src={url} />
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Effect

