import React, { Component } from 'react';
import './Info.css';
import Data from './data/Data';
import Information from './information/Information';
import Effect from './effect/Effect';

class Info extends Component {
    constructor(props){
        super(props)
    }

    changeEffect(newArr) {
        this.props.getNewImg(newArr)
    }

    render() {
        return (
            <div className="Info d-flex">
                <div className="d-flex data-info">
                    <Data />
                </div>
                <div className="d-flex">
                    <Information />
                </div>
                <div className="d-flex">
                    <Effect changes={this.changeEffect.bind(this)} />
                </div>
            </div>
        )
    }
}

export default Info
