import React, { Component } from 'react';
import './Home.css';
import Carousel from './carousel/Carousel';
import Info from './info/Info';

class Home extends Component {
    constructor(){
        super()
        this.state = {
            images: [
                './img/cold.jpg',
                './img/day.jpg',
                './img/warm.jpg'
            ]
        }
    }

    changeImg(images) {
        this.setState({images})
    }

    render() {
        return (
            <div className="Home d-flex">
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-6 parent-section">
                        <div className="carousel-section">
                            <Carousel images={this.state.images} />
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-12 col-lg-6">
                        <div className="data-section">
                            <Info getNewImg={this.changeImg.bind(this)} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home
