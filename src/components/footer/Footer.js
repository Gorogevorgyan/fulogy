import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
    constructor(props){
        super(props);
        this.state = {
            num: ['one', 'two', 'three', 'four','five', 'six', 'seven', 'eight']
        }
    }

    handleEvent = (e)=> {
        let item = e.target;
        let list = document.getElementsByClassName('list-footer-active')[0];
        if(list){
            list.classList.remove('list-footer-active');
        }
        if(item.classList.contains('border-red')){
            item.classList.remove('border-red')
        }
        item.classList.add('list-footer-active', 'border-blue');
        console.log(item, 'ba vor asum ei')
    }

    render() {
        return (
            <footer className="Footer">
                <div className="desktop-footer">
                    <div className="container-fluid d-flex">
                        <div className="list-footer border-blue list-footer-active" onClick={this.handleEvent}>Вариант кухни</div>
                        <div className="list-footer border-red" onClick={this.handleEvent}>Размеры</div>
                        <div className="list-footer border-red" onClick={this.handleEvent}>Сенсор</div>
                        <div className="list-footer border-red" onClick={this.handleEvent}>Питающий кабель</div>
                        <div className="list-footer border-red" onClick={this.handleEvent}>Блок питания</div>
                        <div className="list-footer border-red " onClick={this.handleEvent}>Цвет свечения</div>
                        <div className="list-footer border-red" onClick={this.handleEvent}>Монтаж</div>
                        <div className="list-footer border-red" onClick={this.handleEvent}>Корзина</div>
                    </div>
                </div>
                <div className="mobile-footer">
                    <div id="carouselLines" className="carousel slide">
                        <div className="carousel-inner">
                            <div className="carousel-item">Вариант кухни</div>
                            <div className="carousel-item">Размеры</div>
                            <div className="carousel-item">Сенсор</div>
                            <div className="carousel-item">Питающий кабель</div>
                            <div className="carousel-item">Блок питания</div>
                            <div className="carousel-item active">Цвет свечения</div>
                            <div className="carousel-item">Монтаж</div>
                            <div className="carousel-item">Корзина</div>
                        </div>
                        <a className="carousel-control-prev" href="#carouselLines" role="button"
                           data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#carouselLines" role="button"
                           data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer


