import React, { Component } from 'react';
import './Header.css';
import Nav from './navbar/Nav';

class Header extends Component {
    render() {
        return (
            <header className="Header d-flex align-items-center">
                <Nav />
            </header>
        )
    }
}

export default Header
