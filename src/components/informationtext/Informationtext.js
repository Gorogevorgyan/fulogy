import React from 'react';
import './Informationtext.css';

export default () => {

    const closeInformation = () => {
        const $text = document.getElementsByClassName('text-information')[0]
        const $menu = document.getElementsByClassName('mobile-close-text')[0]
        $menu.classList.add('close-text')
        $text.classList.add('close-text')
    };

    return (
        <div>
            <div className="mobile-close-text close-text">
                <div className="d-flex">
                    <div className="d-flex justify-content-center align-items-center left-section">i</div>
                    <div className="row d-flex justify-content-center align-items-center right-section right-text">
                        <div className="d-flex justify-content-center col-sm-11 text-menu-mob">Вариант кухни</div>
                        <div className="d-flex col-sm-1 close-menu-mob">
                            <i className="fa fa-times icons-size" onClick={closeInformation}></i>
                        </div>
                    </div>
                </div>
            </div>
            <div className="text-information close-text">
                <div>
                    <p className="back" onClick={closeInformation}>{'<<< Back'}</p>
                    is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </div>
            </div>
        </div>
    )
}
