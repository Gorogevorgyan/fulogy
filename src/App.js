import React from 'react';
import './App.css';
import Header from './components/header/Header';
import Sidebar from './components/sidebar/Sidebar';
import Informationtext from './components/informationtext/Informationtext';
import Home from './components/home/Home';
import Footer from './components/footer/Footer';

function App() {
  return (
      <React.Fragment>
          <Header />
          <Home />
          <Sidebar />
          <Informationtext />
          <Footer />
      </React.Fragment>
  );
}

export default App;
